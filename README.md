# Learning the Go Programming Language



## Getting started

Create a Dockerfile in the current directory with an alpine go docker image

```Dockerfile
FROM golang:1.20.1-alpine3.17 as dev

WORKDIR /work

```

Then you have to build the image. Run the build command with the target dev for building the dev stage
```
docker build --target dev -t softcomweb-go .
```

Now you can run the go container you just built and make sure you are in the right project directory because we mount the current project directory into our docker image
```
docker run -v ${PWD}:/work -it softcomweb-go sh
```

## Build Docker image for video app

```
docker build --progress=plain -t softcomweb-video --no-cache .
```
## Run
```
docker run softcomweb-video get -all
```