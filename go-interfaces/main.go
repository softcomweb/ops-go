package main

import (
	"fmt"
)

type Person struct {
	firstname string
	lastname string
	age int
}

type Student struct {
	Person				//Embedding Person struct into student
	studentID int
}

type Employee struct {
	Person
	employeeID int
}

type Worker interface {
	getInfo(t string)
}

func (student Student) getInfo(personType string) {
	fmt.Printf("PersonType: %v\n FirstName: %v\n LastName: %v\n Age: %v\n StudentID: %v\n", personType, student.firstname, student.age, student.lastname, student.studentID)
}

func (employee Employee) getInfo(employeeType string) {
	fmt.Printf("PersonType: %v\n FirstName: %v\n LastName: %v\n Age: %v\n EmployeeID: %v\n", employeeType, employee.firstname, employee.lastname, employee.age, employee.employeeID)
}

func main() {
	student := Student{}
	student.firstname = "Leadel"
	student.lastname = "Kolo"
	student.age = 10
	student.studentID = 101

	employee := Employee{}
	employee.firstname = "Nicole"
	employee.lastname = "Kolo"
	employee.age = 20
	employee.employeeID = 201

	student2 := Student{Person{"leacoco", "kolo", 30}, 303}
	employee2 := Employee{Person{"nic", "kolo", 40}, 303}
	
	worker := [] Worker {student, employee}
	worker2 := [] Worker {student2, employee2}
	
	worker[0].getInfo("STUDENT")
	worker[1].getInfo("EMPLOYEE")

	personStatus := [] string {"STUDENT", "EMPLOYEE"}
	
	for i , workType := range worker2 {
		workType.getInfo(personStatus[i])
	}
}