package main

import (
	"flag"
	"os"
	"log"
	"info.softcomweb/videos/videocontrollers"
)

func main() {

	if len(os.Args) < 2 {
		log.Println("Expecting a sub command \"get\" or \"add\"")
		os.Exit(1)
	}
	command := os.Args[1:]
	sub_command := command[0]
	// parsed_commands := command[1:]

	log.Println("Log command from LOG")
	log.Println("The Sub command is: ", sub_command)
	//get videos subcomand -> videos get
	getCmd := flag.NewFlagSet("get", flag.ExitOnError)

	//input for the get comand ( -all and -id)
	getAll := getCmd.Bool("all", false, "Get all Videos")
	getID := getCmd.String("id", "", "Get Video by ID")

	// Add videos command -> videos add
	// addCmd := flag.NewFlagSet("add", flag.ExitOnError)
	// addID := addCmd.String("id", "", "Video ID")
	// addTitle := addCmd.String("title", "", "Video title")
	// addUrl := addCmd.String("url", "", "Video url")
	// addImageUrl := addCmd.String("imageurl", "", "Video Image url")
	// addDesc := addCmd.String("desc", "", "Video Image description")

	switch sub_command {
	case "get":
		log.Println("Calling the Handle get command")
		HandleGet(getCmd, getAll, getID)
	case "add":
		log.Println("Calling the handle add command")
	default:
		log.Println("Sorry i cannot help you")
		os.Exit(1)
	}

}

func HandleGet(getCmd *flag.FlagSet, all *bool, id *string) {
	getCmd.Parse(os.Args[2:])

	if *all {
		videos := videocontrollers.GetVideos()
		for _, video := range videos {
			log.Println(video)
		}
	}
}
