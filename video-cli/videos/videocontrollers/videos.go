package videocontrollers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type video struct {
	Id          string
	Title       string
	Description string
	Imageurl    string
	Url         string
}

const VIDEOS_PATH = "./videos.json"

// Get all Videos
func GetVideos() (videos []video) {
	fmt.Printf("Reading videos from file path %s \n", VIDEOS_PATH)
	fileBytes, err := ioutil.ReadFile(VIDEOS_PATH)

	if err != nil {
		panic(err)
	}
	fmt.Println("Parsing videos json")
	err = json.Unmarshal(fileBytes, &videos)

	if err != nil {
		panic(err)
	}

	return videos
}

func SaveVideos(videos []video) {
	videoBytes, err := json.Marshal(videos)

	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile(VIDEOS_PATH, videoBytes, 0777)

	if err != nil {
		panic(err)
	}
}
