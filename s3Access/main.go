package main

import (
	"context"
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

func main() {
	//First load the aws config
	cfg, err := config.LoadDefaultConfig(context.TODO(), config.WithSharedConfigProfile("softcomweb"))

	if err != nil {
		log.Fatal("Could not load profile from config")
		panic(err)
	}
	fmt.Println(cfg)
	
	s3_client := s3.NewFromConfig(cfg)

	bucket, err := s3_client.ListBuckets(context.TODO(), &s3.ListBucketsInput{})
	
	if err != nil {
		log.Fatal("Could not list s3 buckets")
		fmt.Printf("%+v",err)
	}

	fmt.Println(bucket)
	
}