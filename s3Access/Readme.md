## Using the aws sdk for go

Before you start, make sure you export the following env variable so as to enable go to read the aws credentials from you home dir

```
export AWS_SDK_LOAD_CONFIG=true

```

Initialize your project and Install the aws go SDK

```
go mod init awsS3Access
go get github.com/aws/aws-sdk-go-v2
go get github.com/aws/aws-sdk-go-v2/config
go get github.com/aws/aws-sdk-go-v2/service/s3

```