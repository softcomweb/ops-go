# A bank go app

# Create a make file

Add @ at the commands in make so not to print them out

# Run make file

make run # This will build the project and place the binary in the bin directory and call the binary with ./bin/gobank

# Packages

use github.com/gorilla/mux
This package is use to implement request router and dispatcher for matching incoming requests to their respective handler

MUX => "HTTP request multiplexer"

# Json representation

type Person struct {
ID int `json:"user_id"`
FirstName string `json:"user_firstname"`
}

The json values are what going to be represented in the json response. Keys can be overwritten be this method

# Storage

In this tutorial we will use the postgres docker image

```
docker run --name softcomweb-postgres -e POSTGRES_PASSWORD=<VERY_SECURE_PASSWORD> -p 5432:5432 -d postgres
```

and to connect

```
psql -h localhost -p 5432 -U postgres

# Enter your very secure password from the above step
```

# Development

Use CompileDaemon during development which will auto build your project everytime you make changes

```
go get github.com/githubnemo/CompileDaemon
go install github.com/githubnemo/CompileDaemon

```

To run the command from the commandline

```
go install github.com/githubnemo/CompileDaemon

```
