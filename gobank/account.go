package main

import "math/rand"

func NewAccount(firstname string, lastname string) *Account {
	return &Account{
		ID: rand.Intn(10000),
		FirstName: firstname,
		LastName: lastname,
		Number: int64(rand.Int()),
	}
}