package main

import "time"

type Account struct {
	ID int `json:"id"`
	FirstName string `json:"firstname"`
	LastName string `json:"lastname"`
	Number int64	`json:"accountNumber"`
	Balance int64	`json:"accountBalance"`
	CreateAt time.Time `json:"createdAt"`
}

type CreateAccountRequest struct {
	FirstName string `json:"firstname"`
	LastName string `json:"lastname"`
}
