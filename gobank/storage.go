package main

import (
	"database/sql"

	_"github.com/lib/pq"
)

type Storage interface {
	CreateAccount(*Account) error
	DeleteAccount(int) error
	UpdateAccount(*Account) error
	GetAccountByID(int) (*Account, error)
}

type PostgresStore struct {
	db *sql.DB
}

func NewPostgresStore() (*PostgresStore, error) {
	connStr := "user=postgres dbname=postgres password=stdpsw sslmode=disable"
	db, err := sql.Open("postgres", connStr)

	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	return &PostgresStore{
		db: db,
	}, nil
}

func (store *PostgresStore) Init() error {
	return nil
}

func (store *PostgresStore) CreaeteAccontTable() error {
	//query := "create table account if not exists"
	return nil
}

func (store *PostgresStore) CreateAccount(*Account) error {
	return nil
}

func (store *PostgresStore) DeleteAccount(id int) error {
	return nil
}

func (store *PostgresStore) UpdateAccount(*Account) error {
	return nil
}

func (store *PostgresStore) GetAccountByID(id int) (*Account, error) {
	return nil, nil
}