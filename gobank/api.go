package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func writeJSON(writer http.ResponseWriter, status int, v any) error {
	writer.Header().Add("Content-Type", "application/json")
	writer.WriteHeader(status)
	return json.NewEncoder(writer).Encode(v)
}

type ApiError struct {
	Error string
}

type apiFunc func(http.ResponseWriter, *http.Request) error

func makeHTTPHandleFunc(f apiFunc) http.HandlerFunc {
	return func(writer http.ResponseWriter, reader *http.Request) {
		if err := f(writer, reader); err != nil {
			writeJSON(writer, http.StatusBadRequest, ApiError{Error: err.Error()})
		}
	}
}

type APIServer struct {
	listenAddr string
	store Storage
}

func NewAPIServer(listenAddr string, store Storage) *APIServer {
	return &APIServer{
		listenAddr: listenAddr,
		store: store,
	}
}

func (server *APIServer) Run() {
	router := mux.NewRouter()

	router.HandleFunc("/account", makeHTTPHandleFunc(server.handleAccount))
	router.HandleFunc("/account/{id}", makeHTTPHandleFunc(server.handleGetAccount))
	//router.HandleFunc("/account/{id}/{firstname}", makeHTTPHandleFunc(server.handleGetAccount))

	log.Println("API Server running on port: ", server.listenAddr)
	http.ListenAndServe(server.listenAddr, router)
}

func (server *APIServer) handleAccount(writer http.ResponseWriter, reader *http.Request) error {
	if reader.Method == "GET" {
		// log.Println("Received Request: ", reader)
		// fmt.Println("############################")
		// log.Println("Received Request BODY: ", reader.Body)
		// fmt.Println("############################")
		// log.Println("Received Request PATH: ", reader.URL.Path)
		// fmt.Println("############################")
		// log.Println("Received Request QUERY: ", reader.URL.Query().Encode())
		// fmt.Println("############################")
		return server.handleGetAccount(writer, reader)
	}

	if reader.Method == "POST" {
		return server.handleCreateAccount(writer, reader)
	}

	if reader.Method == "DELETE" {
		return server.handleDeleteAccount(writer, reader)
	}

	return fmt.Errorf("method [%s] not allowed", reader.Method)
}

func (server *APIServer) handleGetAccount(writer http.ResponseWriter, reader *http.Request) error {
	// account := NewAccount("Leadel", "Kolo")
	// return writeJSON(writer, http.StatusBadRequest, account)

	vars := mux.Vars(reader)
	id := vars["id"]
	//firstname := vars["firstname"]
	idi, err := strconv.Atoi(id)
	if err != nil {
		fmt.Println(("Error during conversion from string to int"))
		panic(err)
	}
	// Make database call for account with ID
	log.Println(mux.CurrentRoute(reader))
	log.Println(id)
	return writeJSON(writer, http.StatusOK, Account{ID: idi})

}

func (server *APIServer) handleCreateAccount(writer http.ResponseWriter, reader *http.Request) error {
	log.Println(reader.Body)
	createAcc := &CreateAccountRequest{}
	if err := json.NewDecoder(reader.Body).Decode(createAcc); err != nil {
		return err
	}

	account := NewAccount(createAcc.FirstName, createAcc.LastName)
	return writeJSON(writer, http.StatusOK, account)
}

func (server *APIServer) handleDeleteAccount(writer http.ResponseWriter, reader *http.Request) error {
	fmt.Println("Implement the Handle Delete Account")
	return nil
}

func (server *APIServer) handleTransfer(writer http.ResponseWriter, reader *http.Request) error {
	return nil
}
